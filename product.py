# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    stock_category = fields.Many2One('product.category', 'Stock category',
        domain=[
            ('stock', '=', True),
            ('stock_uom_category', '=', Eval('default_uom_category', -1))
        ], depends=['default_uom_category'])


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'


class TemplateCategoryAll(metaclass=PoolMeta):
    __name__ = 'product.template-product.category.all'

    @classmethod
    def union_models(cls):
        return super().union_models() + ['product.template']

    @classmethod
    def union_column(cls, name, field, table, Model):
        if Model.__name__ == 'product.template':
            mapping = {
                'template': 'id',
                'category': 'stock_category'
            }
            return super().union_column(mapping.get(name, name),
                field, table, Model)
        return super().union_column(name, field, table, Model)
