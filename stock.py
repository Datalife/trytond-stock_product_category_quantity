# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import PYSONEncoder, PYSONDecoder, Date, Eval, Bool
from trytond.report import Report, datetime
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateView, Button, StateAction,
    StateReport)
from trytond.modules.company.model import CompanyValueMixin
from trytond.tools.multivalue import migrate_property
from trytond.tools import grouped_slice
from trytond import backend

__all__ = ['Configuration', 'ConfigurationDateCriteria',
    'Category', 'ProductsByLocationsStart', 'ProductsByLocations',
    'ProductsByLocationsReport', 'Location']


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    stock_date_criteria = fields.MultiValue(
        fields.Selection([('today', 'Today'),
                          ('previous_month', 'Previous month'),
                          ('max', 'Max date')], 'Stock date criteria',
                         required=True)
    )

    @classmethod
    def default_stock_date_criteria(cls, **pattern):
        return cls.multivalue_model(
            'stock_date_criteria').default_stock_date_criteria()

    @classmethod
    def get_stock_date(cls):
        Date = Pool().get('ir.date')
        res = {
            None: Date.today(),
            'today': Date.today(),
            'previous_month': Date.today().replace(day=1) - datetime.timedelta(
                days=1),
            'max': datetime.datetime.max
        }
        return res[cls(1).stock_date_criteria]


class ConfigurationDateCriteria(ModelSQL, CompanyValueMixin):
    "Configuration Zone"
    __name__ = 'stock.configuration.stock_date_criteria'

    stock_date_criteria = fields.Selection([('today', 'Today'),
                          ('previous_month', 'Previous month'),
                          ('max', 'Max date')], 'Stock date criteria',
                         required=True)

    @classmethod
    def default_stock_date_criteria(cls):
        return'today'

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationDateCriteria, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('stock_date_criteria')
        value_names.append('stock_date_criteria')
        fields.append('company')
        migrate_property('agro.configuration',
            field_names, cls, value_names, fields=fields)


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    quantity = fields.Function(fields.Float('Quantity'), 'sum_product')
    forecast_quantity = fields.Function(fields.Float('Forecast Quantity'),
                                        'sum_product')
    cost_value = fields.Function(fields.Numeric('Cost Value'), 'sum_product')
    stock = fields.Boolean('Stock', select=True,
        states={
            'readonly': Bool(Eval('childs', [0])) | Bool(Eval('parent')),
            },
        depends=['parent'])
    stock_uom_category = fields.Many2One('product.uom.category',
        'Stock Uom category', states={
            'required': Bool(Eval('stock')),
            'invisible': Bool(~Eval('stock')),
            'readonly': (Bool(Eval('parent', None)) & Bool(Eval('childs', [])))
        },
        depends=['stock', 'parent', 'childs'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.parent.domain.append(('stock', '=', Eval('stock', False)))
        cls.parent.depends.append('stock')

    @classmethod
    def sum_product(cls, records, names):
        Product = Pool().get('product.product')

        res = {}
        for name in names:
            res.setdefault(name, {})
            if name not in ('quantity', 'forecast_quantity', 'cost_value'):
                raise Exception('Bad argument')

        for record in records:
            with_childs = Transaction().context.get('with_childs', False)
            _domain = (
                'stock_category',
                'child_of' if with_childs else '=', record.id)
            if with_childs:
                _domain += ('parent',)
            products = Product.search([_domain])
            for name in names:
                sum_ = 0. if name != 'cost_value' else Decimal(0)
                for product in products:
                    sum_ += getattr(product, name)
                res[name].update({record.id: sum_})
        return res

    @classmethod
    def create(cls, vlist):
        id_parent_records = [r['parent'] for r in vlist if r.get('parent')]
        if id_parent_records:
            parent_records = {parent.id: parent
                for parent in cls.search([('id', 'in', id_parent_records)])}
            for vals in vlist:
                parent = parent_records.get(vals.get('parent'), None)
                if parent and parent.stock:
                    vals['stock'] = parent.stock
                    vals['stock_uom_category'] = parent.stock_uom_category

        vlist = super().create(vlist)
        return vlist


class ProductsByLocationsStart(ModelView):
    'Products by Locations'
    __name__ = 'stock.products_by_locations.start'

    forecast_date = fields.Date(
        'At Date', help=('Allow to compute expected '
            'stock quantities for this date.\n'
            '* An empty value is an infinite date in the future.\n'
            '* A date in the past will provide historical values.'))
    categories = fields.One2Many('product.category', None, 'Categories',
        domain=[('stock', '=', True)], required=True)
    with_cat_childs = fields.Boolean('Category children',
        help='Get products and quantities of child categories.')
    group_childs = fields.Boolean('Group category Childs', states={
            'invisible': ~Eval('with_cat_childs')
        }, depends=['with_cat_childs'],
        help='Determines if child categories will be grouped in parent one.')
    child_locations = fields.Boolean('Child locations',
        help='Determines child locations of first level will be detailed.')
    show_empty_stock = fields.Boolean('Show empty stock',
        help='Determines if locations and categories with stock equal '
            'to 0 will be showed.')

    @staticmethod
    def default_forecast_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_with_cat_childs():
        return True

    @fields.depends('with_cat_childs')
    def on_change_with_cat_childs(self):
        if not self.with_cat_childs:
            self.group_childs = False


class ProductsByLocations(Wizard):
    'Products by Locations'
    __name__ = 'stock.products_by_locations'
    start = StateView('stock.products_by_locations.start',
       'stock_product_category_quantity.products_by_locations_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Print', 'print_', 'tryton-print'),
         Button('Open', 'open', 'tryton-ok', default=True)])
    open = StateAction('stock.act_products_by_locations')
    print_ = StateReport('stock.products_by_locations')

    def do_open(self, action):
        pool = Pool()
        Location = pool.get('stock.location')
        Lang = pool.get('ir.lang')

        context = {}
        context['locations'] = Transaction().context.get('active_ids')
        date = self.start.forecast_date or datetime.date.max
        context['stock_date_end'] = Date(date.year, date.month, date.day)
        action['pyson_context'] = PYSONEncoder().encode(context)

        locations = Location.browse(context['locations'])

        for code in [Transaction().language, 'en_US']:
            langs = Lang.search([
                    ('code', '=', code),
                    ])
            if langs:
                break
        lang = langs[0]
        date = lang.strftime(date)

        action['name'] += ' - (%s) @ %s' % (
            ','.join(l.name for l in locations), date)

        if self.start.categories:
            _domain = (
                'stock_category', 'child_of' if self.start.with_cat_childs
                else 'in', list(map(int, self.start.categories)))
            if self.start.with_cat_childs:
                _domain += ('parent', )
            action['pyson_domain'] = PYSONDecoder().decode(
                action['pyson_domain'])
            action['pyson_domain'] = [action['pyson_domain'], [_domain]]
            action['pyson_domain'] = PYSONEncoder().encode(
                action['pyson_domain'])
        return action, {}

    def default_start(self, fields):
        pool = Pool()
        Conf = pool.get('stock.configuration')
        CategoryLocation = pool.get('stock.product.category.location')

        res = {'forecast_date': Conf.get_stock_date()}
        loc_ids = Transaction().context['active_ids']
        default_cats = CategoryLocation.search(
            ['OR', ('warehouse', 'in', loc_ids),
             ('location', 'in', loc_ids)])
        if default_cats:
            res['categories'] = list(set(
                [c.category.id for c in default_cats]))
        return res

    def do_print_(self, action):
        Location = Pool().get('stock.location')

        context = {}
        context['locations'] = Transaction().context.get('active_ids')
        date = self.start.forecast_date or datetime.date.max
        context['stock_date_end'] = Date(date.year, date.month, date.day)
        action['pyson_context'] = PYSONEncoder().encode(context)
        data = {
            'stock_date_end': date,
            'categories': list(map(int, self.start.categories)) or [],
            'with_cat_childs': self.start.with_cat_childs,
            'id': Transaction().context['active_ids'].pop(),
            'group_childs': self.start.group_childs,
            'show_empty_stock': self.start.show_empty_stock,
        }
        data['ids'] = [data['id']]
        if self.start.child_locations:
            data['ids'] = [c.id for c in Location(data['id']).childs]
        return action, data


class ProductsByLocationsReport(Report):
    """Products by location Report"""
    __name__ = 'stock.products_by_locations'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Company = pool.get('company.company')
        Product = pool.get('product.product')
        Category = pool.get('product.category')
        CategoryLocation = pool.get('stock.product.category.location')
        Uom = pool.get('product.uom')

        report_context = super(ProductsByLocationsReport, cls).get_context(
            records, data)

        data.update(cls._get_default_configuration(data))
        report_context['company'] = Company(Transaction().context['company'])
        locations = {}

        cat_ids = data.get('categories', [])
        default_cats = {}
        if 'categories' not in data:
            cats = CategoryLocation.search(['OR',
                ('warehouse', 'in', [r.id for r in records]),
                ('location', 'in', [r.id for r in records])
            ])
            for cat in cats:
                key = cat.location.id if cat.location else cat.warehouse.id
                default_cats.setdefault(key, []).append(cat.category.id)
                cat_ids.append(cat.category.id)

        product_domain = []
        if cat_ids:
            new_domain = ('stock_category',
                'child_of' if data['with_cat_childs'] else 'in',
                cat_ids)
            if data['with_cat_childs']:
                new_domain += ('parent', )
            product_domain.append(new_domain)

        products = Product.search(product_domain)

        context = Transaction().context
        context['stock_date_end'] = data['stock_date_end']
        context['locations'] = [r.id for r in records]

        cat_set = set(cat_ids)
        fpbl, pbl = {}, {}
        for sub_records in grouped_slice(records):
            location_ids = [l.id for l in sub_records]

            def get_products_by_location(_context):
                with Transaction().set_context(_context):
                    return Product.products_by_location(
                            location_ids,
                            grouping=('product',),
                            grouping_filter=([p.id for p in products],),
                            with_childs=context.get('with_childs', True))

            context['forecast'] = False
            pbl.update(get_products_by_location(context))
            context['forecast'] = True
            fpbl.update(get_products_by_location(context))

        def format_qties(values):
            res = {}
            for key, qty in values.items():
                res.setdefault(key[0], {})
                res[key[0]][key[1]] = qty
            return res

        qties = format_qties(pbl)
        forecast_qties = format_qties(fpbl)

        for record in records:
            locations.setdefault(record.id, {None: [0, 0, None]})
            location_qties = qties.get(record.id, {})
            location_fqties = forecast_qties.get(record.id, {})
            record_cats = default_cats.get(record.id, cat_ids)
            child_cat_ids = Category.search([
                ('parent', 'child_of', record_cats)
            ])
            child_cat_ids = [c.id for c in child_cat_ids]
            duom = None
            if products:
                duom = products[0].default_uom
            for product in products:
                if product.id not in location_qties.keys() and \
                        product.id not in location_fqties.keys():
                    continue
                for category in product.categories:
                    if category.id not in record_cats + child_cat_ids:
                        continue
                    cat_set.add(category.id)
                    locations[record.id].setdefault(
                        category, [0, 0, product.default_uom])

                    def add_quantity(_category):
                        locations[record.id][_category][0] += (
                            Uom.compute_qty(
                                product.default_uom,
                                location_qties.get(product.id, 0), duom))
                        locations[record.id][_category][1] += (
                            Uom.compute_qty(
                                product.default_uom,
                                location_fqties.get(product.id, 0), duom))

                    add_quantity(category)
                    add_quantity(None)

            if data['group_childs']:
                cats = Category.browse(cat_ids)
                cat_set = set(cat_ids)
                for cat in cats:
                    total_quantity = 0
                    total_forecast_quantity = 0
                    cats_to_remove = []

                    def is_parent(cat, parent_cat):
                        if cat.parent:
                            if cat.parent.id == parent_cat.id:
                                return True
                            return is_parent(cat.parent, parent_cat)
                        return False

                    for c, v in locations[record.id].items():
                        if c is None:
                            continue
                        if is_parent(c, cat):
                            total_quantity += v[0]
                            total_forecast_quantity += v[1]
                            cats_to_remove.append(c)
                    for c in cats_to_remove:
                        locations[record.id].pop(c)
                    locations[record.id][cat] = [total_quantity,
                        total_forecast_quantity, duom]

            new_locs = [(
                None,
                locations[record.id][None][0],
                locations[record.id][None][1],
                [])
            ]
            new_locs.extend([(k, v)
                for k, v in locations[record.id].items() if k])
            locations[record.id] = sorted(
                new_locs, key=lambda x: x[0] and x[0].rec_name or '')

        report_context['locations'] = locations
        data['stock_date_end'] = data['stock_date_end']
        data['product_categories'] = Category.browse(cat_set)
        return report_context

    @classmethod
    def _get_default_configuration(cls, data):
        Conf = Pool().get('stock.configuration')
        res = data.copy()

        res.setdefault('with_cat_childs', False)
        res.setdefault('stock_date_end', None)
        res.setdefault('group_childs', False)
        if not res['stock_date_end']:
            res['stock_date_end'] = Conf.get_stock_date()
        return res


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    @classmethod
    def get_quantity(cls, locations, name):
        pool = Pool()
        Product = pool.get('product.product')
        Date_ = pool.get('ir.date')
        trans_context = Transaction().context

        if not trans_context.get('product_category'):
            return super().get_quantity(locations, name)

        def valid_context(name):
            return (trans_context.get(name) is not None
                and isinstance(trans_context[name], int))

        if not any(map(valid_context, ['product_category'])):
            return {l.id: None for l in locations}

        context = {}
        if (name == 'quantity'
                and (trans_context.get('stock_date_end', datetime.date.max)
                    > Date_.today())):
            context['stock_date_end'] = Date_.today()

        if name == 'forecast_quantity':
            context['forecast'] = True
            if not trans_context.get('stock_date_end'):
                context['stock_date_end'] = datetime.date.max

        products = [p.id for p in Product.search([
            ('stock_category',
                'child_of', trans_context.get('product_category'), 'parent')])]
        grouping = ('product',)
        grouping_filter = (products or [0],)
        pbl = {}
        for sub_locations in grouped_slice(locations):
            location_ids = [l.id for l in sub_locations]
            with Transaction().set_context(context):
                pbl.update(Product.products_by_location(
                        location_ids,
                        grouping=grouping,
                        grouping_filter=grouping_filter,
                        with_childs=trans_context.get('with_childs', True)))

        res = {l.id: 0 for l in locations}
        for key, value in pbl.items():
            loc_id, _ = key
            res[loc_id] += value
        return res
